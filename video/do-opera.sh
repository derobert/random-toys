# ARGS:
# 	encode - run the encode (default)
# 	test - run a very fast, very low quality encode
# 	dumpseg - Dump segments (to check parsing and to verify encode)
mode=${1:-encode}

wintitle() {
	if [ -n "$STY" ]; then
		# screen seems to choke on Unicode, use ASCII only
		printf "\033k%s\033\\" "$(printf '%s' "$1" | iconv -f UTF-8 -t US-ASCII//TRANSLIT)";
	fi
}

iopt=()
oopt=(-sn -crf ${vo_crf:-25})
case "$mode" in
	encode)
		iopt+=();
		oopt+=(-filter:v "${vf_hq:-bwdif=mode=send_field:deint=all}"
			  -preset slow)
		;;
	test)
		iopt+=(-lowres 3)
		oopt+=(-filter:v "${vf_lq:-scale=w=480:h=-8:sws_flags=fast_bilinear}"
			  -preset ultrafast)
		;;
	dumpseg)
		do_dumpseg=1
		;;
	*)
		echo "Unknown mode: $mode. Known are encode, test, and dumpseg." >&2
		exit 1
		;;
esac

declare -a ffmpeg
if [ 0 -eq ${#ffmpeg[@]} ]; then
	ffmpeg=(ffmpeg)
fi

seglen=${#segments[@]}
declare -a segargs
if [ 0 -ne $((seglen % 2)) ]; then
	echo "ERROR: Segments are start end pairs, can't have an odd number."
	exit 1
fi
[ -n "$do_dumpseg" ] && echo '         INPUT                     OUTPUT'
totlen=0
for ((i = 0; i < seglen; i += 2)) {
	outend=$(echo "$totlen + (${segments[$i+1]} - ${segments[$i]})" | bc)
	human_s=$(dc -e "$totlen d 3600 / n [ ] n d 60 / 60 % n [ ] n 60 % f")
	human_e=$(dc -e "$outend d 3600 / n [ ] n d 60 / 60 % n [ ] n 60 % f")
	printf '% 10.3f : %-10.3f   %10.3f : %-10.3f (%i:%02i:%06.3f – %i:%02i:%06.3f)\n' ${segments[$i]} ${segments[$i+1]} $totlen $outend $human_s $human_e
	totlen=$outend
	segargs+=(-codec:v copy -codec:a copy
	          -ss "${segments[$i]}" -to "${segments[$i+1]}" -f mpeg -)
}
[ -n "$do_dumpseg" ] && exit 0;

set -xe
outfile=GreatPerformances/"$outtitle".mkv
wintitle "Encoding $outtitle..."
${ffmpeg[@]} -accurate_seek -i $infile -loglevel warning "${segargs[@]}" \
	| schedtool -v -n 10 -B -e ${ffmpeg[@]} "${iopt[@]}" -f mpeg -i - \
	"${oopt[@]}" \
	-codec:v libx264 -codec:a copy \
	GreatPerformances/"$outtitle".mkv
wintitle "R128 $outtitle..."
gen-mplayer-r128-files "$outfile"
wintitle 'Done!'
