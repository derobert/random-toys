#!/usr/bin/perl
use 5.024;
use strict;
use warnings qw(all);

use IPC::Run3;
use Bit::Vector;
use File::Temp qw();
use Encode qw(encode decode);

2 == @ARGV
	or die "Usage: $0 edit-info-file output.pdf";

open my $fh, '<:encoding(UTF-8)', $ARGV[0]
	or die "open $ARGV[0]: $!";
my ($pdfs, $ranges, $bookmarks) = parse_file($fh);
close $fh or die "close $ARGV[0]: $!";

while (my ($pdf, $info) = each %$pdfs) {
	check_file_ranges($pdf, $info->{pages}, [ grep($pdf eq $_->{pdf}, @$ranges) ]);
}

my $tmpdir = File::Temp::tempdir(CLEANUP => 1);
run_merge($pdfs, $ranges, "$tmpdir/merged.pdf");
add_bookmarks($bookmarks, "$tmpdir/merged.pdf", $ARGV[1]);

sub add_bookmarks {
	my ($bookmarks, $infile, $outfile) = @_;

	my ($data_enc, $data);
	run3 ['pdftk', $infile, 'dump_data_utf8' ], undef, \$data_enc;
	$? and die "pdftk dump_data_utf8: $?";
	$data = decode('UTF-8', $data_enc, Encode::FB_CROAK);

	foreach my $bookmark (@$bookmarks) {
		$data .= <<BOOKMARK

BookmarkBegin
BookmarkTitle: $bookmark->{title}
BookmarkLevel: $bookmark->{level}
BookmarkPageNumber: $bookmark->{page}
BOOKMARK
	}

	$data_enc = encode('UTF-8', $data, Encode::FB_CROAK);
	run3 ['pdftk', $infile, 'update_info_utf8', '-', 'output', $outfile],
		\$data_enc;
	$? and die "pdftk update_info_utf8: $?";

	return;
}

sub run_merge {
	my ($pdfs, $ranges, $outfile) = @_;

	my @cmd = ( 'pdftk' );
	push @cmd, map("$_->{tag}=$_->{file}", values %$pdfs);

	push @cmd, 'cat';
	foreach my $range (@$ranges) {
		next if 'skip' eq $range->{op};
		my $rtxt = $range->{pdf} . $range->{start};
		$rtxt .= "-$range->{end}" if $range->{start} != $range->{end};
		if ('copy' eq $range->{op}) {
			# default for pdftk, do nothing
		} elsif ($range->{op} =~ /^(?:left|right|down)$/) {
			$rtxt .= $range->{op};
		} else {
			die "Unknown operation: $range->{op}";
		}
		push @cmd, $rtxt;
	}

	push @cmd, 'output', $outfile;

	run3 \@cmd;
	$? and die "pdftk: $?";

	return;
}

sub check_file_ranges {
	my ($pdf, $pages, $ranges) = @_;
	# pages are 1-based, bit vectors are 0-based. Easy solution: just
	# ignore bit 0 (but set it to 1, to make is_full work)
	my $handled = Bit::Vector->new($pages + 1);
	$handled->Bit_On(0);

	foreach my $range (@$ranges) {
		for my $page ($range->{start} .. $range->{end}) {
			$handled->contains($page) and say STDERR "WARNING: PDF $pdf, page $page in more than one range";
			$handled->Bit_On($page);
		}
	}
	if (!$handled->is_full) {
		# find the missing pages
		for my $page (1..$pages) {
			$handled->contains($page) or say STDERR "ERROR: PDF $pdf, page $page is not handled! Use 'skip' to omit it.";
		}
		die "Errors detected; see above.\n";
	}

	return;
}

sub parse_file {
	my $fh = shift;
	local $_;

	my %pdfs;
	my (@ranges, @bookmarks);

	my $state = 'want_in';
	while (<$fh>) {
		chomp;
		/^\s*#/ and next; # comment is valid everywhere
		/^$/ and next; # blank lines are ignored

		if ('want_in' eq $state) {
			/^INPUTS:$/ or die "Unexpected line, expecting INPUTS: $_";
			$state = 'inputs';
		} elsif ('inputs' eq $state) {
			if (/^PAGES:$/) {
				$state = 'pages';
			} elsif (/^([A-Z])\.([0-9]+)=(.+)$/) {
				$pdfs{$1} = {tag => $1, pages => $2, file => $3};
			} else {
				die "Unexpected line, expected A=file.pdf or PAGES: $_";
			}
		} elsif ('pages' eq $state) {
			if (/^BOOKMARKS:$/) {
				$state = 'bookmarks';
			} elsif (/^([A-Z]) \s+ ([0-9]+) \s+ ([0-9]+) \s+ (\w+)$/x) {
				push @ranges, {
					pdf => $1,
					start => $2,
					end => $3,
					op => $4,
				};
			} else {
				die "Unexpected line, expected page range or BOOKMARKS: $_";
			}
		} elsif ('bookmarks' eq $state) {
			/^ \s* ([0-9]+) \s+ ([0-9]+) \s+ (.+) $/x or die "Unexpected line, expecting bookmark: $_";
			push @bookmarks, {page => $2, level => $1, title => $3};
		} else {
			die "Internal error: state machine confused itself (state=$state)"
		}
	}

	return \%pdfs, \@ranges, \@bookmarks;
}
