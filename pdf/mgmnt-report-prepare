#!/usr/bin/perl
use 5.024;
use strict;
use warnings qw(all);

use IPC::Run3;
use File::Temp qw();

my @pdfs;
my $pdf_key = 'A';
foreach my $pdf (@ARGV) {
	my $info;
	run3 [ 'pdfinfo', $pdf ], \undef, \$info;
	$? and die "pdfinfo: $?";

	$info =~ /^Pages: \s+ ([0-9]+)$/mx or die "$pdf: unknown page count";
	my $page_count = $1;

	my @page_ops;
	if ($info =~ /^Creator: \s* RICOH\sMP\sC6502$/mx) {
		# bloody scanner doesn't do this itself, or maybe Joe needs to
		# find the setting...
		my $tmpdir = File::Temp->newdir(
			TEMPLATE => 'mgmt-report-pdf-dump-XXXXXXXX',
			TMPDIR   => 1,
			CLEANUP  => 1
		);

		print STDERR "Dumping $pdf to TIFF files for orientation check... ";
		run3 ['pdftocairo', $pdf, qw(-r 300 -gray -tiff), "$tmpdir/p"];
		print STDERR "done.\n";

		print STDERR "Checking orientation... ";
		opendir(my $dh, $tmpdir);
		my @tiffs = sort grep /^p-/, readdir $dh;
		my @rot;
		foreach my $tiff (@tiffs) {
			$tiff =~ /^p-(\d+)\.tif$/ or die "Unexpected name: $tiff";
			my $page = $1;
			my $orient;
			run3 ['tesseract', "$tmpdir/$tiff", qw(stdout --psm 0 -l osd)], \undef,
				\$orient, \undef;
			if ($orient =~ /^Rotate: (\d+)$/m) {
				print STDERR "${page}[$1] ";
				$rot[$page - 1] = $1;
			} else {
				print STDERR "${page}[??] ";
				$rot[$page - 1] = 0;
			}
		}
		print STDERR "done.\n";

		# all this is with 0-based page numbers, but we convert to
		# 1-based for @page_ops
		my $rstart = 0;          # range start
		my $rrot   = $rot[0];    # range rotation
		for (my $n = 1; $n < @rot; ++$n) {
			next if $rrot == $rot[$n];
			push @page_ops, [1 + $rstart, $n, get_rotate_name($rrot)];
			$rstart = $n;
			$rrot = $rot[$n];
		}
		push @page_ops, [1 + $rstart, $page_count, get_rotate_name($rrot)];
	} else {
		push @page_ops, [1, $page_count, 'copy'];
	}

	push @pdfs, {
		file     => $pdf,
		key      => $pdf_key++,
		pages    => $page_count,
		page_ops => \@page_ops,
	};
}

print <<INPUTS;
INPUTS:
# no editing should be needed here.
INPUTS
say join("\n", map("$_->{key}.$_->{pages}=$_->{file}", @pdfs));

printf(qq{\nPAGES:\n#F%4s %4s  op (copy, left, right, down, skip)\n},
	qw(st end));
foreach my $pdf (@pdfs) {
	foreach my $range (@{$pdf->{page_ops}}) {
		printf(qq{%s %4i %4i  %s\n}, $pdf->{key}, @$range);
	}
}

print <<BOOK;

BOOKMARKS:
# Note: bookmarks are *after* joining. So there is no file letter.
#l  pg   title
1    1   Bookmark Title
BOOK

sub get_rotate_name {
	my $rot = shift;
	return (
		  0 == $rot   ? 'copy'
		: 90 == $rot  ? 'right'
		: 180 == $rot ? 'down'
		: 270 == $rot ? 'left'
		:               die "Unknown rotation $rot"
	);
}
