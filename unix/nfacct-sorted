#!/usr/bin/perl
use 5.024;
use warnings qw(all);
use strict;
use IPC::Run3;
use Number::Format qw(format_number);
use Text::ASCIITable;
use utf8;

# TODO: should make a proper library to parse nfacct output someday.
# This code is copied from nfacct-bwmon and slightly modified (we don't
# care about the original order since we sort).

my ($out, $err);
run3 ['nfacct', 'list'], \undef, \$out, \$err;
die "Unexpected stderr: $err" if '' ne $err;

my %res;
foreach (split "\n", $out) {
	/^\{\s*([^}]+?)\s*\} = (.+);$/ or die "unparsable: $_";
	my %dat = split(q/\s*(?:,|=)\s*/, $1);
	$res{$2} =  { split(q/\s*(?:,|=)\s*/, $1) };
}

my $t = Text::ASCIITable->new(
	{hide_FirstLine => 1, hide_HeadLine => 0, hide_LastLine => 0});
$t->setCols('Host', 'Bytes', 'Packets');

foreach my $host (
	sort {
		    $res{$b}{bytes} <=> $res{$a}{bytes}
			|| $res{$b}{pkts} <=> $res{$a}{pkts}
			|| $a cmp $b
	} keys %res
	)
{
	$t->addRow($host, format_number(0 + $res{$host}{bytes}),
		              format_number(0 + $res{$host}{pkts}));
}

binmode STDOUT, ':utf8';
print STDOUT $t->draw(
	['', '', '', ''],
	['│', "│ @{[ scalar localtime ]}", '│'],
	['┝', '┥', '━', '┿'],
	['│', '│', '│'],
	['└', '┘', '━', '┷'],
	# ['└', '┘', '─', '┴'], # alternate, for no pseudo-drop-shadow
);
