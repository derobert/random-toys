#!/usr/bin/perl

use 5.024;
use warnings qw(all);
use strict;

use Curses;
use IPC::Run3;
use Data::Dump qw(pp);
use Time::HiRes qw(clock_gettime clock_nanosleep CLOCK_MONOTONIC);
use List::Util qw(max);

sub get_current_data {
	my ($out, $err);
	local $_;

	# could get the clock before or after or both and weight them.
	# picked before because assuming that most of the time is fork/exec
	# (incl. loading binary from disk, etc.) and not the printing.
	run3 ['nfacct', 'list'], \undef, \$out, \$err;
	my $when = clock_gettime(CLOCK_MONOTONIC);
	die "Unexpected stderr: $err" if '' ne $err;

	my (@order, %res);
	foreach (split "\n", $out) {
		/^\{\s*([^}]+?)\s*\} = (.+);$/ or die "unparsable: $_";
		push @order, $2;
		$res{$2} =  { split(q/\s*(?:,|=)\s*/, $1) };
	}

	return $when, \@order, \%res;
}

sub calc_rate {
	my ($item, $now, $then) = @_;

	my $delta_t = $now->[0] - $then->[0];
	
	my $then_dat = $then->[1]{$item}{bytes}
		or return '';
	my $now_dat = $now->[1]{$item}{bytes}
		or return '!ERR'; # WTF, should not happen

	my $res = ($now_dat - $then_dat)*8.0/$delta_t/1000;
	return $res < 0.01 ? '' : sprintf('%9.1f', $res);
}

sub loop {
	my $continue = 1;
	$SIG{INT} = sub { $continue = 0 };

	# Approach: We use CLOCK_MONOTONIC to keep each array entry as close
	# to 1s as possible, but more importantly to smooth out any
	# variations, so $history[60] really will be 60s ago (0-based array
	# indexes, but 0 is now).

	my $want_next = 1 + clock_gettime(CLOCK_MONOTONIC);
	my @data;

	initscr;
	curs_set(0);

	my @intervals = qw(1 10 60 300 900 3600);

	while ($continue) {
		my ($when, $order, $new_data) = get_current_data();
		push @data, [$when, $new_data];
		shift @data if @data > 3601;

		erase;
		attron(A_BOLD);
		addstr(0, 0, 'Entry');
		for (my $i = 0; $i < @intervals; ++$i) {
			addstr(0, 10*(2+$i), sprintf('%9s', 'kbps@'.$intervals[$i]));
		}
		attroff(A_BOLD);
		for (my $i = 0; $i < @$order; ++$i) {
			my $h = $order->[$i];
			addstr(1+$i, 0, $h);
			for (my $j = 0; $j < @intervals; ++$j) {
				my $int = $intervals[$j];
				addstr(1+$i, 10*(2+$j),
					calc_rate($h, $data[-1], $data[-1 - $int])) if @data > $int;
			}
		}
		
		refresh;

		# just in case, force a delay of at least 100ms. Otherwise we'd
		# peg if we get behind.
		my $delay = max(0.1, $want_next - clock_gettime(CLOCK_MONOTONIC));
		clock_nanosleep(CLOCK_MONOTONIC, 1_000_000_000 * $delay);
		$want_next += 1;
	}

	endwin;
}

loop();
