#!/usr/bin/perl
# In git at random-toys/backup/
use 5.024;
use warnings qw(all);
use strict;

use DBI;
use IPC::Run3;
use File::Path qw(make_path);
use POSIX qw(strftime);
use autodie qw(rename);

our $EXPIRED_POOL = 'S3QL-Expired';
our $STORAGE_DIR = '/mnt/S3QL/bacula';
our $EXPIRED_DIR = strftime('/mnt/S3QL/expired-bacula/%Y-%m', localtime);
our $DSN = 'dbi:Pg:dbname=bacula';

# make sure director is idle
say STDERR "Waiting until director is idle... ";
run3 [ '/usr/sbin/bconsole' ], \qq{wait\nexit\n}, undef, undef;
say STDERR "Director idle; continuing.";

my $dbh = DBI->connect($DSN, '', '',
	{AutoCommit => 0, RaiseError => 1, ShowErrorStatement => 1});

make_path($EXPIRED_DIR, { chmod => 02770, group => 'bacula' });

my $sth = $dbh->prepare(<<QUERY);
	SELECT volumename
	  FROM media
	 WHERE poolid=(SELECT poolid FROM pool WHERE name=?)
	   AND volstatus='Purged'
	 ORDER BY mediaid
QUERY
$sth->execute($EXPIRED_POOL);

say STDERR "Moving expired files...";
my @cmds;
while (my ($vol) = $sth->fetchrow_array) {
	if ( -f "$STORAGE_DIR/$vol" ) { 
		rename("$STORAGE_DIR/$vol", "$EXPIRED_DIR/$vol");
		push @cmds, qq{delete volume="$vol" pool="$EXPIRED_POOL" yes\n};
	} else {
		warn "File for volume $vol does not exist";
	}
}
push @cmds, "exit\n";
say STDERR "done.";

$sth->finish;
$dbh->rollback; # just in case
$dbh->disconnect;

say STDERR "Deleting expired volumes from Bacula...";
run3 [ '/usr/sbin/bconsole' ], \@cmds, undef, undef;
say STDERR "done.";
