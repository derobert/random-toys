#!/usr/bin/perl
use 5.028;
use warnings qw(all);
use strict;

use Carp;
use DateTime;
use DateTime::Format::ISO8601;
use DBI qw(SQL_BLOB SQL_INTEGER SQL_VARCHAR);
use Digest::SHA qw();
use File::chdir;
use File::Find;
use File::Spec::Functions qw(abs2rel);
use IO::AIO;
use IPC::Run3 qw(run3);
use List::Util qw(reduce);
use List::MoreUtils qw(natatime);
use Getopt::Long;

use Data::Dump qw(pp);
use Devel::Size qw(total_size);

# Note we don't ignore .conf files because not all of them were just
# pre-mpv-volnorm volume adjustments. Some were things like aspect ratio
# fixes.
our $conf_ignore = qr!
	(?: (?: / (?: WAITING | SAME-SUBS ) (?: \. [-_ a-z A-Z 0-9]+ )? $ )
	  | (?: / \. thumbnails / )
	)!xx;
our $conf_index = '/media/anthony/BigBackup/index';
our $conf_db    = "$conf_index/.db/bigbackup-indexer-test.sqlite";

use constant {
	SCAN_COMMIT_INTERVAL        => 30,
	BTRFS_EXTENT_SAME_MAX_FILES => 64    # seems the kernel limit is 100
};

our $DBH;
our ($current_trunk, $current_bough);


sub go {
	my $mode;
	my sub set_mode {
		my $new_mode = shift;
		return sub {
			my $opt = shift;
			defined $mode
				and die "Can't have two modes, $mode and $new_mode ($opt)";
			$mode = $new_mode
		};
	}
	GetOptions(
		'scan'    => set_mode('scan'),
		'clean'   => set_mode('clean'),
		'dedup'   => set_mode('dedup'),
		'rebuild' => set_mode('rebuild'));
	defined $mode or die "No mode set.\n";

	db_connect();
	for ($mode) {
		/^scan$/    and return go_scan();
		/^dedup$/   and return go_dedup();
		/^clean$/   and return go_clean();
		/^rebuild$/ and return go_rebuild();

		confess "BUG: unknown mode $_";
	}
	$DBH->rollback; # commands should commit if needed
}

sub go_scan {
	my $boughs = get_boughs();

	# Could use aio_scandir to do this, but seems silly to re-implement
	# File::Find. Not to mention, when trying the obvious way... you
	# have to be careful not to run out of file descriptors (it does put
	# an impressive load on the system, though!) Really just using
	# IO::AIO because it has an easy FIEMAP.
	#
	# Start, though, by setting the maximum outstanding requests. Each
	# one costs at least one fd.
	IO::AIO::max_outstanding 64;

	my $next_commit = time + SCAN_COMMIT_INTERVAL;
	foreach my $trunk (@ARGV) {
		print STDERR "Processing trunk $trunk... ";
		$current_trunk = get_trunk($trunk);
		foreach $current_bough (@$boughs) {
			find({
					wanted   => \&process_item,
					no_chdir => 1,                # otherwise a nightmare with AIO
				},
				"$trunk/$current_bough->{bough_path}"
			);
		}
		if (time >= $next_commit) {
			print STDERR "commit... ";
			$DBH->commit;
			$next_commit = time + SCAN_COMMIT_INTERVAL;
		}
		say STDERR "done.";
	}

	print STDERR "Finishing... ";
	IO::AIO::flush;
	print STDERR "commit... ";
	$DBH->commit;
	say STDERR "done.";

	print STDERR "Hashing same name, different extents... ";
	hash_duplicate_names();
	$DBH->commit;
	say STDERR "done.";

	# FIXME / TODO: if one file with a given leaf_no is hashed, all of
	# them need to be. At least if it has null extents_no

	print STDERR "DB optimize... ";
	$DBH->do(q{PRAGMA optimize});
	say STDERR "finished.";

	return 0;
}

sub db_connect {
	$DBH = DBI->connect(
		"dbi:SQLite:dbname=$conf_db",
		'', '',
		{
			RaiseError         => 1,
			ShowErrorStatement => 1,
			AutoCommit         => 0,
			FetchHashKeyName   => 'NAME_lc'
		});
	$DBH->do(q{PRAGMA foreign_keys=ON});
	return;
}

sub get_trunk {
	my $path = shift;
	$path =~ s!/+$!!; # no trailing slashes

	get_X(
		table      => 'trunks',
		pkey_col   => 'trunk_no',
		path_col   => 'trunk_path',
		path_val   => $path,
		extra_cols => ['trunk_date'],
		extra_bind => sub {
			my ($sth, $first_pnum) = @_;
			if ($path
				=~ m!/(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[-+]\d{2}):?(\d{2})$!a)
			{
				# weird pattern is because parse_datetime requires zone as
				# +-HH:MM not +-HHMM like date -Is used to print.
				my $dt = DateTime::Format::ISO8601->parse_datetime("$1:$2");
				$sth->bind_param($first_pnum, $dt->epoch, SQL_INTEGER);
			} else {
				confess "Could not parse date from $path";
				# TODO use stat if name parse fails?
			}
			return;
		});
}

sub get_branch {
	my $path = shift;
	state %cache;

	$path =~ s!(?: ^/+ ) | (?: /+$ )!!gx; # no leading or trailing slashes

	return $cache{$path} if exists $cache{$path};
	return $cache{$path} = get_X(
		table      => 'branches',
		pkey_col   => 'branch_no',
		path_col   => 'branch_path',
		path_val   => $path,
	);

}

sub get_twig {
	my $path = shift;
	state %cache;

	$path =~ s!(?: ^/+ ) | (?: /+$ )!!gx; # no leading or trailing slashes

	return $cache{$path} if exists $cache{$path};
	return $cache{$path} = get_X(
		table      => 'twigs',
		pkey_col   => 'twig_no',
		path_col   => 'twig_path',
		path_val   => $path,
		raw_select => 't2.bundle_path AS bundle_path',
		raw_join   => 'LEFT JOIN bundles t2 ON (t1.bundle_no = t2.bundle_no)'
	);

}

sub get_leaf {
	my $path = shift;
	state %cache;

	$path =~ m!/$!
		and confess("Something wrong, leaf has trailing slash: $path");
	$path =~ s!^/+!!;    # no leading slashes

	return $cache{$path} if exists $cache{$path};
	return $cache{$path} = get_X(
		table    => 'leaves',
		pkey_col => 'leaf_no',
		path_col => 'leaf_path',
		path_val => $path
	);
}

sub get_extents {
	my $extents = shift;
	state %cache;

	defined $extents or return {};

	return $cache{$extents} if exists $cache{$extents};
	return $cache{$extents} = get_X(
		table     => 'extents',
		pkey_col  => 'extents_no',
		path_col  => 'extents',
		path_val  => $extents,
		path_type => SQL_VARCHAR,
	);
}

sub get_hash {
	my $hash = shift;
	state %cache;

	defined $hash or return {};

	return $cache{$hash} if exists $cache{$hash};
	return $cache{$hash} = get_X(
		table     => 'hashes',
		pkey_col  => 'hash_no',
		path_col  => 'hash',
		path_val  => $hash,
		path_type => SQL_VARCHAR,
	);
}

sub get_hash_fh {
	my $fh = shift;

	my $dgst = Digest::SHA->new('SHA-512/256')
		or die "Could not init SHA-512/256";
	$dgst->addfile($fh);

	return get_hash('S252:' . $dgst->b64digest);
}

sub get_X { # all programs shall contain at least a nano-ORM
	my %opts = @_;
	my $table      = $opts{table}      // croak(q{'table' required});
	my $pkey_col   = $opts{pkey_col}   // croak(q{'pkey_col' required});
	my $path_col   = $opts{path_col}   // croak(q{'path_col' required});
	my $path_val   = $opts{path_val}   // croak(q{'path_val' required});
	my $path_type  = $opts{path_type}  // SQL_BLOB;
	my $extra_cols = $opts{extra_cols} // [];
	my $extra_bind = $opts{extra_bind} // sub { };
	my $inserted   = $opts{_inserted}; # internal use only!

	# and then the nano-ORM grows...
	my @raw_select = defined $opts{raw_select} ? ($opts{raw_select}) : ();
	my $raw_join = $opts{raw_join} // '';

	my $query;
	$query = <<QUERY;
SELECT
    ${ \join(q{, }, $pkey_col, $path_col, @$extra_cols, @raw_select) }
  FROM $table t1 $raw_join
  WHERE $path_col = ?
QUERY
	my $sth = $DBH->prepare_cached($query);
	$sth->bind_param(1, $path_val, $path_type);
	$sth->execute;
	my $row = $sth->fetchrow_hashref;
	$sth->finish;
	return $row if $row;
	confess "Did not find row after inserting it" if $inserted;

	$query = <<QUERY;
INSERT INTO $table(${ \join(q{, }, $path_col, @$extra_cols) })
  VALUES ( ${ \join(q{, }, ('?')x(1 + @$extra_cols)) })
QUERY
	$sth = $DBH->prepare_cached($query);
	$sth->bind_param(1, $path_val, $path_type);
	$extra_bind->($sth, 2);
	$sth->execute;

	return get_X(@_, _inserted => 1);
}

sub get_boughs {
	my $sth = $DBH->prepare(<<QUERY);
SELECT bough_no, bough_path, bough_prio
  FROM boughs
QUERY

	$sth->execute;
	return $sth->fetchall_arrayref( {} );
}

sub print_flag_values {
	# unused code, for dev/debug
	foreach my $flag (qw(
		IO::AIO::FIEMAP_EXTENT_LAST IO::AIO::FIEMAP_EXTENT_UNKNOWN
		IO::AIO::FIEMAP_EXTENT_DELALLOC IO::AIO::FIEMAP_EXTENT_ENCODED
		IO::AIO::FIEMAP_EXTENT_DATA_ENCRYPTED IO::AIO::FIEMAP_EXTENT_NOT_ALIGNED
		IO::AIO::FIEMAP_EXTENT_DATA_INLINE IO::AIO::FIEMAP_EXTENT_DATA_TAIL
		IO::AIO::FIEMAP_EXTENT_UNWRITTEN IO::AIO::FIEMAP_EXTENT_MERGED
		IO::AIO::FIEMAP_EXTENT_SHARED
		))
	{
		printf qq{Flag %-38s %-6d\n}, $flag, eval $flag;
	}
}

use constant EXPECTED_FIEMAP_FLAGS =>
	(IO::AIO::FIEMAP_EXTENT_LAST | IO::AIO::FIEMAP_EXTENT_SHARED
		| IO::AIO::FIEMAP_EXTENT_DATA_INLINE
		| IO::AIO::FIEMAP_EXTENT_NOT_ALIGNED);
use constant FIEMAP_NOT_PHYSICAL => (IO::AIO::FIEMAP_EXTENT_DATA_INLINE
		| IO::AIO::FIEMAP_EXTENT_NOT_ALIGNED);

sub process_item {
	m/$conf_ignore/ and return;
	-f   or return;    # we care only of files
	-s _ or return;    # empty files are not interesting

	# capture these for the closure
	my $file = $_; 
	my $trunk = $current_trunk;
	my $bough = $current_bough;

	aio_open $file, IO::AIO::O_RDONLY, 0, sub {
		my $fh = shift;
		unless ($fh) {
			warn "open $file: $!";
			return;
		}
		aio_fiemap $fh, 0, undef, IO::AIO::FIEMAP_FLAG_SYNC, undef, sub {
			my $emap = shift;
			my $ored_flags = reduce { $a | $b->[3] } 0, @$emap;
			if ($ored_flags & ~EXPECTED_FIEMAP_FLAGS) {
				warn "Unexpected flags for $file: $ored_flags (bravely continuing)";
			}

			$ored_flags & IO::AIO::FIEMAP_EXTENT_LAST
				or die "File $file: did not get last extent, bailing out";

			my ($physical, $hash);
			if ($ored_flags & FIEMAP_NOT_PHYSICAL) {
				# if not physical, have to hash it to detect duplicates.
				$hash = get_hash_fh($fh);
			} else {
				local $_;
				# not entirely sure its always sorted, so sort it.
				$physical = join(
					q{:},
					map(sprintf('%x+%x', $_->[1], $_->[2]),
						sort({ $a->[0] <=> $b->[0] } @$emap)));
			}

			# full path:
			#    /media/anthony/BigBackup/snap/2019-02-20T05:27:15-05:00/backup/srv_videos/Sorted/00_Show_Complete/Kero Kero Chime/Kero Kero Chime - 01 [AC].avi
			# becomes:
			#    trunk: /media/anthony/BigBackup/snap/2019-02-20T05:27:15-05:00
			#    bough: backup/srv_videos/Sorted
			#    branch: 00_Show_Complete
			#    twig: Kero Kero Chime
			#    leaf: Kero Kero Chime - 01 [AC].avi

			$file =~ m!^
				\Q$trunk->{trunk_path}\E /+
				\Q$bough->{bough_path}\E /+
				(?: (?<branch> .+ ) /+ )?   # branch is optional
				(?<twig> [^/]+ ) /+
				(?<leaf> [^/]+ )
			$!x or $file =~ m!^
				\Q$trunk->{trunk_path}\E /+
				\Q$bough->{bough_path}\E /+
				(?<leaf> [^/]+ )
			$!x or confess "Could not parse $file";

			my $branch = get_branch($+{branch} // '.');
			my $twig = get_twig($+{twig} // '.');
			my $leaf = get_leaf($+{leaf});
			my $extents = get_extents($physical); # handles undef

			my ($inum, $size) = (stat $file)[1,7];

			my $sth = $DBH->prepare_cached(<<QUERY);
INSERT INTO files (
    trunk_no, bough_no, branch_no, twig_no, leaf_no,
    inode, size, extents_no, hash_no
  ) VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9)
  ON CONFLICT (
    leaf_no, twig_no, trunk_no, branch_no, bough_no
  ) DO UPDATE SET
    inode = :6,
    size = :7,
    extents_no = :8,
    hash_no = :9
QUERY
			$sth->bind_param(1, $trunk->{trunk_no}, SQL_INTEGER);
			$sth->bind_param(2, $bough->{bough_no}, SQL_INTEGER);
			$sth->bind_param(3, $branch->{branch_no}, SQL_INTEGER);
			$sth->bind_param(4, $twig->{twig_no}, SQL_INTEGER);
			$sth->bind_param(5, $leaf->{leaf_no}, SQL_INTEGER);
			$sth->bind_param(6, $inum, SQL_INTEGER);
			$sth->bind_param(7, $size, SQL_INTEGER);
			$sth->bind_param(8, $extents->{extents_no}, SQL_INTEGER);
			$sth->bind_param(9, $hash->{hash_no}, SQL_INTEGER);
			$sth->execute;
			$sth->finish;
		};
	};

	IO::AIO::poll_cb;

	return;
}

sub hash_duplicate_names {
	# The assumption here is that two files with the same name are
	# reasonably likely to be duplicates — since this is a backup of
	# fairly uniquely named files.
	#
	# Also, we only have to compute the hash for a unique set of extents
	# once; files which share the same sectors are clearly the same.
	# (And if all files that have the same name have the same extents,
	# we don't need to hash them at all — this is the majority of files,
	# thankfully).

	# must fully grab this; the updates we're going to do conflict
	# (which seems to get sqlite to just ignore them, without any error
	# or anything...)
	my $work = $DBH->selectall_arrayref(
		q{SELECT file_no, extents_no, known_hash, full_path FROM unhashed_dup_cand});
	printf STDERR "(total=%i) ", scalar(@$work);

	my $sth = $DBH->prepare('UPDATE files SET hash_no = ? WHERE extents_no = ? AND hash_no IS NULL');
	my $next_commit = time + SCAN_COMMIT_INTERVAL;
	while (my ($idx, $row) = each @$work) {
		my ($file_no, $extents_no, $known_hash, $path) = @$row;

		$sth->bind_param(2, $extents_no, SQL_INTEGER);

		if (defined $known_hash) {
			# already hashed a duplicate of this, we know the hash
			$sth->bind_param(1, $known_hash, SQL_INTEGER);
		} else {
			open my $fh, '<', $path
				or die "Could not open $path: $!";
			my $hash = get_hash_fh($fh);
			close $fh;
			$sth->bind_param(1, $hash->{hash_no}, SQL_INTEGER);
		}

		$sth->execute;

		if (time >= $next_commit) {
			printf STDERR "%i ", 1+$idx;
			$DBH->commit;
			$next_commit = time + SCAN_COMMIT_INTERVAL;
		}
	}

	return;
}

sub go_clean {
	print STDERR "Cleaning unused rows:\n";
	clean_unused_rows();
	say STDERR "done.";

	print STDERR "Vacuuming... ";
	{
		# Can not vacuum with transaction open, so switch back to
		# autocommit temporarily.
		local $DBH->{AutoCommit} = 1;
		$DBH->do('VACUUM');
	}
	say STDERR "done.";

	return 0;
}

sub clean_unused_rows {
	my @tables = (
		[ 'extents', 'extents_no' ],
		[ 'hashes',  'hash_no'    ],
	);

	foreach my $rec (@tables) {
		my ($table, $col) = @$rec;
		my $query = <<QUERY;
DELETE FROM $table
 WHERE $col IN (SELECT a.$col
                  FROM $table a
                       LEFT JOIN files f ON (a.$col = f.$col)
                  WHERE f.file_no IS NULL)
QUERY
		print STDERR "   $table... ";
		my $rows = $DBH->do($query);
		$DBH->commit;
		printf STDERR "%i rows removed.\n", $rows;
	}

	return;
}

sub go_dedup {
	local $_;

	say STDERR <<NOTICE;
NOTE: This will use "sudo" to run btrfs-extent-same on all the files
      that are duplicate (according to hash) but do not share extents.
      You may be prompted for a password, depending on your sudo config.

WARNING: You really ought to re-scan all snapshots after this, as the
         extent tracking information will no longer be correct. Worst
         case, a new file uses the space freed by the deduplicate in a
         way that exactly matches a previous file, causing
         bigbackup-indexer to think they're identical. Then or or the
         other would be missing from the generated symlink index.
NOTICE
	# TODO: re-scan the files, we know which ones they are!
	
	# Collect results into data structure:
	#   $tmp{hash_no}{extents_no} = [ full_path1, full_path2, ... ]
	my %tmp;
	push @{$tmp{$_->[0]}{$_->[1]}}, $_->[2]
		foreach $DBH->selectall_array(
			q{SELECT hash_no, extents_no, full_path FROM duplicate_files});

	# Transform into
	#   @work = (
	#     [ # hash 1
	#       [ full_path1, full_path2, ... ],
	#       [ full_path_n, full_path_n+1, ... ]
	#     ],
	#     [ # (the same, but for hash 2)
	#   )
	# where the largest extents_no group comes first inside each hash
	# group. Remember that lists in scalar context (like <=>) yield item
	# count.
	my @work;
	while (my ($h_no, $e_grps) = each %tmp) {
		push @work, [ sort { @$b <=> @$a } values %{$e_grps} ];
	}
	undef %tmp;

	printf STDERR "Deduplicating %i sets of files...\n", scalar(@work);
	while (my ($hg_no, $hash_grp) = each @work) {
		my $exemplar = $hash_grp->[0][0];
		my $size = -s $exemplar;
		defined $size or die "Could not stat $exemplar: $!";

		while (my ($eg_no, $ext_grp) = each @$hash_grp) {
			next if 0 == $eg_no; # ignore exemplar's group

			my $iter = natatime BTRFS_EXTENT_SAME_MAX_FILES, @$ext_grp;
			my $subgroup_no = 0;
			while (my @files = $iter->()) {
				printf STDERR "Hash set %i, extents group %i.%i...\n", 1+$hg_no, 1+$eg_no, $subgroup_no++;

				# Originally used btrfs-extent-same here, but btrfs has
				# so many odd limitations on it that it's a PITA. At
				# some point, for example, it seems to have started
				# requiring requests for large files to be split...
				#
				# So switch to duperemove's fdupes mode, because
				# duperemove already knows all this stuff. Only downside
				# is the file list is now newline-delimited, so
				# arbitrary names no longer work.
				#
				# And you'll need to change your sudo setup.

				my $fdupes_fmt = join("\n", $exemplar, @files, '', '');
				run3([qw(sudo duperemove -A --fdupes)], \$fdupes_fmt);
				$? and
					die "duperemove failed with exit code ${ \($?>>8) }";
			}
		}
	}


	return 0;
}

sub sanitize_path_component {
	local $_ = shift;
	s/^\./(dot)/;
	s/^\s/(ws)/;
	return $_;
}

sub go_rebuild {
	my $sth = $DBH->prepare(<<QUERY);
SELECT twig_final_path,
       leaf_path, bough_prio,
       branch_prio,
       hash_no,
       extents_no,
       full_path -- will give arbitrary one in group, doesn't matter which
FROM joined_files
WHERE branch_ignore = 0 AND file_ignore = 0
GROUP BY twig_final_path, leaf_path, bough_prio, branch_prio, hash_no,
         extents_no
ORDER BY twig_final_path, leaf_path, bough_prio DESC, branch_prio DESC
QUERY
	$sth->execute;

	my %work;
	while (my $row = $sth->fetchrow_hashref) {
		my $ident
			= defined $row->{hash_no}    ? "h\@$row->{hash_no}"
			: defined $row->{extents_no} ? "e\@$row->{extents_no}"
			:   die "No content address for file $row->{full_path}";
		# only take first = highest prio (due to ORDER BY)
		$work{sanitize_path_component($row->{twig_final_path})}
			{sanitize_path_component($row->{leaf_path})}
			{$ident} //= $row->{full_path};
	}

	# find existing dirs and links
	my %existing;
	local $CWD = $conf_index;
	find({
			wanted => sub {
				if (/^\./) {
					# ignore dot file/dirs; we mangle all those away...
					# so any here aren't from us.
				} elsif (-l $_) {
					defined($existing{$File::Find::dir =~ s!^\./!!r}{$_} = readlink)
						or die "readlink($_): $!";
				} elsif (-d $_) {
					$existing{$_} = {}; # make sure we note empty dirs
				}
			},
		},
		'.'
	);

	while (my ($dir, $names) = each %work) {
		-d $dir or mkdir $dir or die "mkdir($dir): $!";
		$existing{$dir}{'!!!SENTINEL!!!'} = undef; # mark dir as wanted
		local $CWD = $dir;
		while (my ($name, $contents) = each %$names) {
			my $n_contents = keys %$contents;
			while (my ($addr, $fullpath) = each %$contents) {
				# up to seven characters after dot, e.g., ".tar.bz2" or
				# e.g., ".srt.zip"
				my $mangled
					= ($n_contents > 1)
					? $name =~ s/((?:\.\S{1,7})?)$/ [$addr]$1/r
					: $name;
				my $target = abs2rel($fullpath);
				if (exists $existing{$dir}{$mangled}) {
					# delete returns deleted value; no work if it's right
					next if $target eq delete $existing{$dir}{$mangled};

					# otherwise, get rid of it
					unlink($mangled) or die "unlink($mangled): $!"; 
				}
				symlink($target, $mangled)
					or die "symlink to $mangled: $!";
			}
		}
	}

	# clean up no longer wanted links and directories
	while (my ($dir, $entries) = each %existing) {
		if (keys %$entries) {
			local $CWD = $dir;
			foreach my $link (keys %$entries) {
				next if '!!!SENTINEL!!!' eq $link;
				-l $link or die "$link is not a symlink";
				unlink($link) or die "unlink($link): $!";
			}
		}

		# if someone tossed a file in here, rmdir might fail, so warn
		# instead of die.
		exists $entries->{'!!!SENTINEL!!!'}
			or rmdir($dir)
			or warn("rmdir $dir: $!");
	}

	return 0;
}

exit go();
