-- Verify bigbackup-indexer:view-useful_file on sqlite

BEGIN;

SELECT 
    file_no,
    trunk_no, trunk_path, trunk_date,
    bough_no, bough_path, bough_prio,
    branch_no, branch_path,
    twig_no, twig_path, bundle_no,
    bundle_path, twig_final_path,
    leaf_no, leaf_path,
    inode, size, extents
FROM joined_files WHERE 0;

SELECT * FROM unhashed_dup_cand WHERE 0;
SELECT hash_no, full_path FROM duplicate_files WHERE 0;

ROLLBACK;
