-- Verify bigbackup-indexer:files on sqlite

BEGIN;

SELECT  trunk_no,   trunk_path,  trunk_date  FROM trunks    WHERE 0;
SELECT  bough_no,   bough_path,  bough_prio  FROM boughs    WHERE 0;
SELECT  branch_no,  branch_path              FROM branches  WHERE 0;
SELECT  bundle_no,  bundle_path              FROM bundles   WHERE 0;
SELECT  twig_no,    twig_path,   bundle_no   FROM twigs     WHERE 0;
SELECT  leaf_no,    leaf_path                FROM leaves    WHERE 0;
SELECT  extents_no, extents                  FROM extents   WHERE 0;
SELECT  hash_no,    hash                     FROM hashes    WHERE 0;

SELECT
    file_no, trunk_no, bough_no, branch_no, twig_no, twig_override,
    leaf_no, inode, extents_no, hash_no, file_ignore
  FROM files
  WHERE 0;

ROLLBACK;
