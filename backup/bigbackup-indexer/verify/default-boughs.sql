-- Verify bigbackup-indexer:default-boughs on sqlite

BEGIN;

-- You'd think something like this would work, blowing up with a divide
-- by 0, but it turns out SQLite just returns no rows instead, without
-- even a warning...
--
-- SELECT 1/COUNT(*)
--   FROM boughs
--  WHERE bough_path = 'backup/srv_videos/TORRENT' AND bough_prio = 100 ;
--
-- oh well, this is data that is intended to be edited if need be
-- anyway.

ROLLBACK;
