-- Revert bigbackup-indexer:files from sqlite

BEGIN;

DROP TABLE files;
DROP TABLE extents;
DROP TABLE hashes;
DROP TABLE leaves;
DROP TABLE twigs;
DROP TABLE bundles;
DROP TABLE branches;
DROP TABLE boughs;
DROP TABLE trunks;

COMMIT;
