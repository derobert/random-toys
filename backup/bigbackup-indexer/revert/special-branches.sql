-- Revert bigbackup-indexer:special-branches from sqlite

PRAGMA foreign_keys = ON;
BEGIN;

DELETE FROM branches WHERE branch_no BETWEEN 1 AND 9;

COMMIT;
