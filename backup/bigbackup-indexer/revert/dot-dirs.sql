-- Revert bigbackup-indexer:dot-dirs from sqlite

BEGIN;

DELETE FROM twigs WHERE twig_no = 1;
DELETE FROM bundles WHERE bundle_no = 1;

COMMIT;
