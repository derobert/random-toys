-- Revert bigbackup-indexer:view-useful_file from sqlite

BEGIN;

DROP VIEW joined_files;
DROP VIEW unhashed_dup_cand;
DROP VIEW duplicate_files;

COMMIT;
