-- Revert bigbackup-indexer:default-boughs from sqlite

PRAGMA foreign_keys = ON;
BEGIN;

DELETE FROM boughs WHERE bough_path IN (
    CAST('backup/srv_videos/TORRENT' AS BLOB),
    CAST('backup/srv_videos/NON_ANIME' AS BLOB),
    CAST('backup/srv_videos/NEWS' AS BLOB),
    CAST('backup/srv_videos/Sorted' AS BLOB)
);

COMMIT;
