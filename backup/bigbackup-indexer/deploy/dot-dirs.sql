-- Deploy bigbackup-indexer:dot-dirs to sqlite
-- requires: initial

PRAGMA foreign_keys = ON;
BEGIN;

INSERT INTO bundles  VALUES (1, CAST('00_Unsorted' AS BLOB));
INSERT INTO twigs    VALUES (1, CAST('.' AS BLOB), 1);

COMMIT;
