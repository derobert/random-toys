-- Deploy bigbackup-indexer:files to sqlite

PRAGMA foreign_keys = ON;
BEGIN;

-- We use BLOB for paths because we don't want e.g., Unicode conversions
-- done on them; they aren't guaranteed to be UTF-8. At least not on
-- Unix.

-- Example of how this all breaks down (which is done to reduce the
-- otherwise massive amount of text duplication that'd occur, BigBackup
-- currently has nearly eight million files):
--   full path:
--      /media/anthony/BigBackup/snap/2019-02-20T05:27:15-05:00/backup/srv_videos/Sorted/00_Show_Complete/Kero Kero Chime/Kero Kero Chime - 01 [AC].avi
--   becomes:
--      trunk: /media/anthony/BigBackup/snap/2019-02-20T05:27:15-05:00
--      bough: backup/srv_videos/Sorted
--      branch: 00_Show_Complete
--      twig: Kero Kero Chime
--      leaf: Kero Kero Chime - 01 [AC].avi
--   which has inode 110852 and extents
--   1123782fd000+7f40000:11238d391000+281000, if it matters.

CREATE TABLE trunks (
	trunk_no       INTEGER   NOT NULL PRIMARY KEY,
	trunk_path     BLOB      NOT NULL UNIQUE,
	trunk_date     INTEGER   NOT NULL UNIQUE
);

CREATE TABLE boughs (
	bough_no       INTEGER   NOT NULL PRIMARY KEY,
	bough_path     BLOB      NOT NULL UNIQUE,
	bough_prio     INTEGER   NOT NULL UNIQUE
);

CREATE TABLE branches (
	branch_no      INTEGER   NOT NULL PRIMARY KEY,
	branch_path    BLOB      NOT NULL UNIQUE,
	branch_ignore  INTEGER   NOT NULL DEFAULT 0,
	branch_prio    INTEGER   NOT NULL DEFAULT 0,
	CONSTRAINT boolean_branch_ignore CHECK ( branch_ignore IN (0,1) )
);

CREATE TABLE bundles (
	-- bundles allow merging multiple twigs (shows) together, in case we
	-- get slightly different directory names. Separate table to make
	-- sure that recursive ones are not possible (and definitely not
	-- infinitely recursive)
	bundle_no      INTEGER   NOT NULL PRIMARY KEY,
	bundle_path    BLOB      NOT NULL UNIQUE
);

CREATE TABLE twigs (
	twig_no        INTEGER   NOT NULL PRIMARY KEY,
	twig_path      BLOB      NOT NULL UNIQUE,
	bundle_no      INTEGER   NULL REFERENCES bundles ON DELETE SET NULL
);

CREATE TABLE leaves ( 
	leaf_no        INTEGER   NOT NULL PRIMARY KEY,
	leaf_path      BLOB      NOT NULL UNIQUE
);

CREATE TABLE extents (
	extents_no     INTEGER   NOT NULL PRIMARY KEY,
	extents        TEXT      NOT NULL UNIQUE
);

CREATE TABLE hashes (
	hash_no        INTEGER   NOT NULL PRIMARY KEY,
	hash           TEXT      NOT NULL UNIQUE
);

CREATE TABLE files (
	file_no        INTEGER   NOT NULL PRIMARY KEY,
	trunk_no       INTEGER   NOT NULL REFERENCES trunks    ON DELETE CASCADE,
	bough_no       INTEGER   NOT NULL REFERENCES boughs    ON DELETE CASCADE,
	branch_no      INTEGER   NOT NULL REFERENCES branches  ON DELETE CASCADE,
	twig_no        INTEGER   NOT NULL REFERENCES twigs     ON DELETE CASCADE,
	twig_override  INTEGER       NULL REFERENCES twigs     ON DELETE SET NULL,
	leaf_no        INTEGER   NOT NULL REFERENCES leaves    ON DELETE CASCADE,
	inode          INTEGER   NOT NULL,
	size           INTEGER   NOT NULL,
	extents_no     INTEGER       NULL REFERENCES extents   ON DELETE NO ACTION,
	hash_no        INTEGER       NULL REFERENCES hashes    ON DELETE NO ACTION,
	file_ignore    INTEGER   NOT NULL DEFAULT 0,
	CONSTRAINT boolean_file_ignore CHECK ( file_ignore IN (0,1) ),

	-- and the non-surrogate primary key:
	UNIQUE(leaf_no, twig_no, trunk_no, branch_no, bough_no)
);

-- these are used by the two duplicate file views & also cleanup
CREATE INDEX files_extents_no_idx ON files(extents_no);
CREATE INDEX files_hash_no_idx ON files(hash_no)
       WHERE hash_no IS NOT NULL;

COMMIT;
