-- Deploy bigbackup-indexer:view-useful_file to sqlite
-- requires: initial
--
-- NOTE: Left joins are used here (even on NOT NULL foreign keys)
-- because otherwise SQLite's optimizer doesn't drop out the table
-- access when you don't select the columns coming from the table (inner
-- join normally requires checking the row exists even if nothing comes
-- from the table, but the foreign keys already require that). Of
-- course, foreign keys in SQLite are optional, so maybe that's why it
-- doesn't. It doesn't even with pragma foreign_keys=1, so... just use
-- outer joins.
--
-- Note, also, the program uses this view.

BEGIN;

CREATE VIEW joined_files AS
  SELECT
    f.file_no,
    f.trunk_no, tr.trunk_path, tr.trunk_date,
    f.bough_no, bo.bough_path, bo.bough_prio,
    f.branch_no, br.branch_path, br.branch_ignore, br.branch_prio,
    f.twig_no, tw.twig_path, tw.bundle_no,
    bu.bundle_path, COALESCE(bu.bundle_path, tw.twig_path) AS twig_final_path,
    f.leaf_no, l.leaf_path,
    f.inode, f.size,
    f.extents_no, e.extents,
    f.hash_no, h.hash,
    f.file_ignore,
    tr.trunk_path || '/' || bo.bough_path || '/' || br.branch_path
                  || '/' || tw.twig_path  || '/' || l.leaf_path AS full_path
  FROM
    files f 
    LEFT JOIN trunks tr ON (f.trunk_no = tr.trunk_no)
    LEFT JOIN boughs bo ON (f.bough_no = bo.bough_no)
    LEFT JOIN branches br ON (f.branch_no = br.branch_no)
    LEFT JOIN twigs tw ON (f.twig_no = tw.twig_no)
    LEFT JOIN bundles bu ON (tw.bundle_no = bu.bundle_no)
    LEFT JOIN leaves l ON (f.leaf_no = l.leaf_no)
    LEFT JOIN extents e ON (f.extents_no = e.extents_no)
    LEFT JOIN hashes h ON (f.hash_no = h.hash_no)
  ;

CREATE VIEW unhashed_dup_cand AS
  SELECT jf.file_no, jf.extents_no, jf.full_path,
         (SELECT f.hash_no
            FROM files f
           WHERE jf.extents_no = f.extents_no AND f.hash_no IS NOT NULL
           LIMIT 1) AS known_hash
    FROM joined_files jf
   WHERE jf.file_no IN (SELECT min(file_no)
                       FROM files
                      WHERE hash_no IS NULL
                            AND leaf_no IN (SELECT leaf_no
                                              FROM files
                                             GROUP BY leaf_no
                                            HAVING count(DISTINCT extents_no) > 1)
                      GROUP BY extents_no)
   ORDER BY jf.trunk_date -- some hope this is in disk order
 ;

CREATE VIEW duplicate_files
AS     SELECT hash_no, extents_no, full_path
       FROM   joined_files
       WHERE  hash_no IN (SELECT hash_no
                          FROM   files
                          WHERE  hash_no        IS NOT NULL
                                 AND extents_no IS NOT NULL
                          GROUP  BY hash_no
                          HAVING count(distinct extents_no) > 1)
;

-- by using various indent styles, insanity is ensured.

COMMIT;
