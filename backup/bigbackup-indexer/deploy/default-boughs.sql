-- Deploy bigbackup-indexer:default-boughs to sqlite
-- requires: initial

BEGIN;

INSERT INTO boughs(bough_path, bough_prio) VALUES
	( CAST('backup/srv_videos/TORRENT' AS BLOB), -500 ),
	( CAST('backup/srv_videos/NON_ANIME' AS BLOB), -100 ),
	( CAST('backup/srv_videos/NEWS' AS BLOB), 0 ),
	( CAST('backup/srv_videos/Sorted' AS BLOB), 500 );

COMMIT;
