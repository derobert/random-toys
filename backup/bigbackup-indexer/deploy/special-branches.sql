-- Deploy bigbackup-indexer:special-branches to sqlite
-- requires: initial

BEGIN;

INSERT INTO branches(branch_no, branch_path, branch_ignore, branch_prio)
  VALUES (1, CAST('.'                AS BLOB), 0, 0)
  ,      (2, CAST('01_No_ReBurn'     AS BLOB), 0, -50)
  ,      (3, CAST('00_Show_Complete' AS BLOB), 0, 500)
  ,      (4, CAST('***RESERVED*4***' AS BLOB), 0, 0)
  ,      (5, CAST('***RESERVED*5***' AS BLOB), 0, 0)
  ,      (6, CAST('***RESERVED*6***' AS BLOB), 0, 0)
  ,      (7, CAST('***RESERVED*7***' AS BLOB), 0, 0)
  ,      (8, CAST('***RESERVED*8***' AS BLOB), 0, 0)
  ,      (9, CAST('***RESERVED*9***' AS BLOB), 0, 0)
  ;


COMMIT;
