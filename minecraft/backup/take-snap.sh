#!/bin/bash
# Maintained in git ssh://git.derobert.net/srv/git/random-toys at
# minecraft/backup

cmd="$HOME/io/cmd"
out="$HOME/io/out"
data="$HOME/data"
snap="$HOME/backup/world"
export BUP_DIR="$HOME/backup/bup/$(date +%Y-%m)"

case "$1" in
	bup)
		mode=bup
		;;
	zip)
		mode=zip
		;;
	*)
		echo "Unknown mode: $1"
		exit 1
		;;
esac

# bup mode: Make sure this month's repository is present.
[ $mode != bup ] || [ -d "$BUP_DIR" ] || bup init

function oh_noes_cleanup {
	echo "Emergency cleanup! Re-enabling world saving"
	echo 'save-on' > "$cmd"
	echo 'say Snapshot FAILED' > "$cmd"
	exit 1
}

trap oh_noes_cleanup EXIT

exec < <(tail -fn0 "$out" --pid=$$)

echo "say World snapshot in progress ($mode)" > "$cmd"
#until grep -q '\[Server] World snapshot in progress'; do true; done

echo "save-all" > "$cmd"
#until grep -q 'CONSOLE: Save complete.'; do true; done
#until grep -q '\[INFO\] Saved the world'; do true; done
#until grep -q '\[Server thread/INFO\]: Saved the world'; do true; done
until grep -q '\[Server thread/INFO\]: Saved the game'; do true; done

echo "save-off" > "$cmd"
#until grep -q 'CONSOLE: Disabled level saving'; do true; done
#until grep -q '\[INFO\] Turned off world auto-saving'; do true; done
#until grep -q '\[Server thread/INFO\]: Turned off world auto-saving'; do true; done
until grep -q '\[Server thread/INFO\]: Automatic saving is now disabled'; do true; done

echo "save-all" > "$cmd"
#until grep -q 'CONSOLE: Save complete.'; do true; done
#until grep -q '\[INFO\] Saved the world'; do true; done
#until grep -q '\[Server thread/INFO\]: Saved the world'; do true; done
until grep -q '\[Server thread/INFO\]: Saved the game'; do true; done

case $mode in
	zip)
		pushd "$data" > /dev/null || exit 1
		zip -q -9 "$snap/$(date +%Y-%m-%d_%H-%M-%S).zip" -r world* || exit 1
		popd > /dev/null || exit 1
		;;
	bup)
		bup index -- "$data"
		bup save -n take-snapshot -9 -- "$data"
		;;
	*)
		echo "internal error: unknown mode $mode"
		exit 127
		;;
esac

echo "save-on" > "$cmd"
#until grep -q 'CONSOLE: Enabled level saving'; do true; done
#until grep -q '\[INFO\] Turned on world auto-saving'; do true; done
#until grep -q '\[Server thread/INFO\]: Turned on world auto-saving'; do true; done
until grep -q '\[Server thread/INFO\]: Automatic saving is now enabled'; do true; done

echo "say World snapshot finished" > "$cmd"
#until grep -q '\[Server] World snapshot finished'; do true; done

trap - EXIT

